from app import app
from apistar import TestClient

client = TestClient(app)


def test_root_200():
    response = client.get('/')
    print(response)
    assert response.status_code == 200


def test_docx_upload():
    files = {'file': open('/data/contract.docx', 'rb')}
    response = client.post('/upload', files=files)
    print(response)
    assert response.status_code == 200
