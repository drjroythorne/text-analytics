# Thoughtriver text analytics

## Running

Needs rabbitmq and mongo Docker containers running locally.

### Mongo

Start mongo container and expose the default port

	docker run --name some-mongo -p 27017:27017 -d mongo

Connect to it and run the mongo shell from another container

	docker run -it --link some-mongo:mongo --rm mongo sh -c 'exec mongo "$MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT/test"'^C

Need to create a db called thoughtriver

	use thoughtriver

### RabbitMQ

	docker run -d --hostname thoughtriver-mq --name some-rabbit -e RABBITMQ_DEFAULT_USER=thoughtriver -e RABBITMQ_DEFAULT_PASS=password -p 5672:5672 -p 8080:15672 rabbitmq:3-management

Management console then available at localhost:8080

### Web bit

	pipenv run apistar run --port 8081

### Task workers

	pipenv run dramatiq -p 1 thoughtriver.broker thoughtriver.extract


## Random postgres noodlings

Starting up postgres and adding initial users and password
	sudo -u postgres psql
	sudo -u postgres createuser <username>
	sudo -u postgres createdb <dbname>

	$ sudo -u postgres psql
	psql=# alter user <username> with encrypted password '<password>';
	psql=# grant all privileges on database <dbname> to <username> ;

