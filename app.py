from thoughtriver import broker
from thoughtriver import db
from apistar.frameworks.wsgi import WSGIApp as App
from thoughtriver.web.routes import routes
settings = {
    'STATICS': {
        'ROOT_DIR': '/data/thoughtriver/static',       # Include the 'statics/' directory.
        'PACKAGE_DIRS': ['apistar']  # Include the built-in apistar static files.
    }
}
db_client = db.connect_db()
app = App(routes=routes, settings=settings)
