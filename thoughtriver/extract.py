import re
import os.path
import dramatiq
import spacy
from spacy import displacy


@dramatiq.actor
def count_words(text):
    count = len(re.findall(r'\w+', text))
    print(f'There are {count} words in the document')
    return count


class SyntaxProcessor:
    def __init__(self, static_dir):
        self.static_dir = static_dir
        self.nlp = spacy.load('en_core_web_sm')

    def __call__(self, text, request_id):
        doc = self.nlp(text)
        html = SyntaxProcessor._spacy_doc_to_html(doc)
        filename = self._make_html_filename(request_id)
        with open(filename, 'w') as f:
            f.write(html)
        return filename

    @staticmethod
    def _spacy_doc_to_html(doc):
        return displacy.render(doc, style='dep', jupyter=False, page=True)

    def _make_html_filename(self, request_id):
        return os.path.join(self.static_dir, request_id+'.html')


syntax_processor = SyntaxProcessor('/data/thoughtriver/static')


@dramatiq.actor
def process(text, request_id):
    return syntax_processor(text, request_id)