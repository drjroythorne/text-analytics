from mongoengine import connect, Document, StringField

def connect_db(db='thoughtriver'):
    client = connect(db)
    return client


class Docx(Document):
    text = StringField(required=True)

