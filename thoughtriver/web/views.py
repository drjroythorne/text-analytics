from apistar import annotate, reverse_url, http
from apistar.parsers import MultiPartParser
import uuid

from ..document import load_text_from_document
from ..extract import count_words, process
from ..db import Docx
from apistar import reverse_url


def welcome():
    return {'message': 'Text processing server'}


@annotate(parsers=[MultiPartParser()])
def upload_docx(data: http.RequestData) -> http.Response:
    file = data['file']
    content = load_text_from_document(file)
    docx = Docx(text=content)
    docx.save()
    word_count = count_words(content)
    request_id = str(uuid.uuid1())
    result_url = reverse_url('serve_static', path=request_id+'.html')
    job = process.send(content, request_id)
    return http.Response({'data': content, 'word_count': word_count, 'result_url': result_url})
