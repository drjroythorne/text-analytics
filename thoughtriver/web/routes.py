from apistar import Route
from apistar.handlers import serve_static
from .views import welcome, upload_docx

routes = [
            Route('/', 'GET', welcome),
            Route('/upload', 'POST', upload_docx),
            Route('/result/{path}', 'GET', serve_static)
]
