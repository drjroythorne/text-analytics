from docx import Document

def load_text_from_document(f):
    doc = Document(f)
    paragraphs = doc.paragraphs
    text = '\n'.join([p.text for p in paragraphs if len(p.text) > 0])
    return text