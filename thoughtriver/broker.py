import dramatiq
from dramatiq.brokers.rabbitmq import RabbitmqBroker
url = "amqp://thoughtriver:password@127.0.0.1:5672"
broker = RabbitmqBroker(url=url)
dramatiq.set_broker(broker)
