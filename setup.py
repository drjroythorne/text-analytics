from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize

extensions = [
    Extension(
        'thoughtriver.calculate',
        ['thoughtriver/calculate.pyx'],
        #include_dirs=[],
        #libraries=[],
        #library_dirs=[]
    ),
]

setup(
        name = 'thoughtriver',
        packages = find_packages(),
        ext_modules = cythonize(extensions)
)
